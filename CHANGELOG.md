# Changelog

## Unreleased

### Fixed

- #4484 Améliorer un peu l’affichage de certaines erreurs JS lors du téléversement d’un fichier

## 3.3.4 — 2024-01-12

### Fixed

- #4897 Une coquille pouvait casser la compression du JS

## 3.3.3 — 2024-01-08

### Security

- #4897 Éviter une XSS basé sur le nom des fichiers uploadés

### Added

- #4889 Ajout d'une vue de la saisie, pour utilisation en saisie PHP avec le plugin saisies

### Fixed

- #4893 Utiliser des `button` au lieu de `span`
- #4888 La configuration de taille maximum des fichiers téléversés est bien en `Mio`
- spip/spip#3637 Lever l'ambiguité sur les balises simples des `url()`

## 3.3.2 — 2023-06-10

### Fixed

- #4878 On réintègre les EXIF dans l'image après la compression en javascript
- #4859 Corriger un bug d'upload lors de l'utilisation d'un NFS

## 3.3.1 — 2023-04-16

### Fixed

- Nom de fichier erronné (mauvaise casse)

## 3.3.0 — 2023-04-11

### Added

- Autoloading PSR-4 Spip\Bigup
- Installable en tant que package Composer

### Changed

- Nécessite SPIP 5.0.0-dev minimum
- Compatible SPIP 5.0.0-dev
